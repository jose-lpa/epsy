import os

import pytest
from falcon import testing

from progimage import config
from progimage.app import api


@pytest.fixture(scope='module')
def client():
    yield testing.TestClient(api)

    # Tear down: delete all contents of uploaded images directory.
    media_location = config.STORAGE_BASE_URI
    for file_name in os.listdir(media_location):
        file_path = os.path.join(media_location, file_name)
        os.unlink(file_path)
