import importlib
import os


STORAGE_BASE_URI = os.getenv('STORAGE_BASE_URI')
STORAGE_BACKEND = getattr(
    importlib.import_module(
        os.getenv('STORAGE_BACKEND', 'progimage.storages')
    ),
    'LocalStorage'
)
