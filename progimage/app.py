import falcon

from progimage import config
from progimage.resources import ImageResource, ImagesResource


api = application = falcon.API()

api.add_route('/images', ImagesResource(config.STORAGE_BACKEND))
api.add_route('/images/{name}', ImageResource(config.STORAGE_BACKEND))
