import json
import mimetypes
from typing import Type

import falcon

from progimage.storages import Storage
from progimage.validators import validate_format, validate_image_type


class ImagesResource:
    def __init__(self, image_storage: Type[Storage]):
        self.image_storage = image_storage()

    def on_get(self, request: falcon.Request, response: falcon.Response):
        payload = {
            'results': [image for image in self.image_storage.list_images()]
        }

        response.body = json.dumps(payload, ensure_ascii=False)
        response.status = falcon.HTTP_200

    @falcon.before(validate_image_type)
    def on_post(self, request: falcon.Request, response: falcon.Response):
        name = self.image_storage.save(request.stream, request.content_type)

        response.status = falcon.HTTP_201
        response.body = json.dumps({'image': name})


class ImageResource:
    def __init__(self, image_storage: Type[Storage]):
        self.image_storage = image_storage()

    @falcon.before(validate_format)
    def on_get(
            self,
            request: falcon.Request,
            response: falcon.Response,
            name: str
    ):
        response.content_type = mimetypes.guess_type(name)[0]

        try:
            response.stream, response.content_length = \
                self.image_storage.retrieve_image(name)
        except IOError:
            raise falcon.HTTPNotFound()
